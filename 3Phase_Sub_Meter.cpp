
#include <EmonLib.h>
#include <XBee.h>
#include <LiquidCrystal.h>

//*************************CONSTANT DEFINITION********************************************
//general parameters
#define NUM_PAR  5  //number of parameters (V,I,P,pf)
#define TIME_DELAY 500 
//measurment paramteres
#define NUM_ACROSS 100  //proportional to number of samples for 1 measurement. 
#define VOLTAGE_CAL 68.43
#define CURRENT_CAL 11.28  //10.75//11.53//11.57

//communication parameters
#define NUM_TX_FRAME_BYTE 14//number of bytes in a transmit frame (without the data bytes)
//LCD parameters
#define PIN_BUTTON2 15
#define DISPLAY_LENGTH 12
//********************************END*****************************************************

//***************************VARIABLE DECLARATION*****************************************
XBee xbee= XBee();          //Xbee variable

float data[NUM_PAR*3+1];    //data
static double energy;       //energy
static int loopcount=0;     
int flag=0;
unsigned long start;

void displaylcd (float var[]);
LiquidCrystal lcd(16, 17, 18, 19, 20, 21);

void setup(){
  Serial.begin(9600);
  xbee.setSerial(Serial);
  
  pinMode(PIN_BUTTON2, INPUT);
  lcd.begin(16, 2);
  energy = 0;
  digitalWrite(10,LOW);
  start=millis();
}

void loop(){
  //digitalWrite(10,LOW);
  // unsigned long start=millis();
  //*************************************************************************************
  EnergyMonitor emon1;        //Emon variable   
  //3 phase V I P measurement
  for (int i=0;i<3;i++){
    emon1.voltage(i+8,VOLTAGE_CAL);          // Voltage: input pin, calibration,  68.43
    emon1.current(i+11,CURRENT_CAL );       // Current: input pin, calibration. 12.94
    
    emon1.phase(0);            //phase caliberation
    
    emon1.calcVI(NUM_ACROSS,2000);         // Calculate all. No.of half wavelengths (crossings), time-out
    energy += emon1.realPower * (millis()-start)/1000.0/3600.0;//W*h

    start=millis();

    data[NUM_PAR*i]     = emon1.realPower;        //extract Real Power into variable
    data[NUM_PAR*i+1]   = emon1.apparentPower;    //extract Apparent Power into [
    data[NUM_PAR*i+2]   = emon1.powerFactor;      //extract Power Factor into Variable
    data[NUM_PAR*i+3]   = emon1.Vrms;             //extract Vrms into Variable
    data[NUM_PAR*i+4]   = emon1.Irms;             //extract Irms into Variable
  }
  data[NUM_PAR*3]  = energy;

  Serial.println("Phase\tRP\tAP\tPF\tV\tI\tenergy");
  for(int i=0;i<3;i++){
    Serial.print("Phase");
    Serial.print(i+1);
    Serial.print("\t");
    for(int j=0;j<NUM_PAR;j++){
      Serial.print(data[NUM_PAR*i+j]);
      Serial.print("\t");
    }
    if(i==2)
      Serial.print(data[NUM_PAR*3]);
    Serial.println(); 
  }
  

  displaylcd(data);//display lcd data

  XBeeAddress64 addr64 = XBeeAddress64(0x00000000,0x00000000);//Set up address
  ZBTxRequest zbTx;//packet variable
  ZBTxStatusResponse txStatus = ZBTxStatusResponse();//response status variable
  uint8_t payload[sizeof(data)];//payload

  while(flag==0){

    for(int i=0;i<sizeof(data)/sizeof(float);i++)
      for(int j=0;j<sizeof(float);j++)
        payload[i*sizeof(float)+j] =((byte *)&data[i])[j];      //transform float type to byte to store data in payload
    zbTx=ZBTxRequest(addr64,payload,sizeof(payload));

    xbee.send(zbTx);

    xbee.readPacket(1000);
    if(xbee.getResponse().isAvailable()){
      flag=1;
      if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE){
        xbee.getResponse().getZBTxStatusResponse(txStatus);
        Serial.print("delivery status: ");
        Serial.println(txStatus.getDeliveryStatus());
      }
      else if (xbee.getResponse().isError()) {
        Serial.println();
        Serial.print("Error reading packet.  Error code: ");  
        Serial.println(xbee.getResponse().getErrorCode());
      }
      delay(500);

    }
    else{
      Serial.println("No response");
      delay(5000);
    }

  }
  flag=0;
}


void displaylcd (float var [])

{
  int state_button2 = digitalRead(PIN_BUTTON2);


  lcd.setCursor(DISPLAY_LENGTH*0,0);
  lcd.print("P1=      W");
  lcd.setCursor(DISPLAY_LENGTH*0+3,0);
  lcd.print(var[0]);

  lcd.setCursor(DISPLAY_LENGTH*1,0);
  lcd.print("P2=      W");
  lcd.setCursor(DISPLAY_LENGTH*1+3,0);
  lcd.print(var[NUM_PAR*1]);
  
  lcd.setCursor(DISPLAY_LENGTH*2,0);
  lcd.print("P3=      W");
  lcd.setCursor(DISPLAY_LENGTH*2+3,0);
  lcd.print(var[NUM_PAR*2]);
  
  lcd.setCursor(DISPLAY_LENGTH*0,1);
  lcd.print("PF1=      ");
  lcd.setCursor(DISPLAY_LENGTH*0+4,1);
  lcd.print(var[2]);

  lcd.setCursor(DISPLAY_LENGTH*1,1);
  lcd.print("PF2=      ");
  lcd.setCursor(DISPLAY_LENGTH*1+4,1);
  lcd.print(var[NUM_PAR*1+2]);
  
  lcd.setCursor(DISPLAY_LENGTH*2,1);
  lcd.print("PF3=      ");
  lcd.setCursor(DISPLAY_LENGTH*2+4,1);
  lcd.print(var[NUM_PAR*2+2]);


  // scroll 13 positions (string length) to the left 
  // to move it offscreen left:
  if(HIGH == state_button2)
  {
    for (int positionCounter = 0; positionCounter < 15; positionCounter++) {
      // scroll one position left:
      lcd.scrollDisplayLeft(); 
      // wait a bit:
      delay(500);
    }

    // scroll 29 positions (string length + display length) to the right
    // to move it offscreen right:
    for (int positionCounter = 0; positionCounter < 20; positionCounter++) {
      // scroll one position right:
      lcd.scrollDisplayRight(); 
      // wait a bit:
      delay(500);
    }
  }


}


// Base Station Arduino Code
#include <XBee.h>

float data[16];
char temp[sizeof(float)];
XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
// create reusable response objects for responses we expect to handle 
ZBRxResponse rx = ZBRxResponse();
ModemStatusResponse msr = ModemStatusResponse();

void setup()
{
  //This intinializes the serial port
  Serial.begin(9600);
  xbee.setSerial(Serial);
  
}

void loop()

{
 
    xbee.readPacket();
   
    if (xbee.getResponse().isAvailable()) {
      // got something
       //Serial.print("xbee.getResponse().isAvailable()");
      
      if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
        // got a zb rx packet
       // Serial.print("ZB_RX_RESPONSE");
        // now fill our zb rx class
        xbee.getResponse().getZBRxResponse(rx);
            
        /*if (rx.getOption() == ZB_PACKET_ACKNOWLEDGED) {
            // the sender got an ACK
            flashLed(statusLed, 10, 10);
        } else {
            // we got it (obviously) but sender didn't get an ACK
            flashLed(errorLed, 2, 20);
        }*/
        // set dataLed PWM to value of the first byte in the data
        //analogWrite(dataLed, rx.getData(0));
      } else if (xbee.getResponse().getApiId() == MODEM_STATUS_RESPONSE) {
        xbee.getResponse().getModemStatusResponse(msr);
        
        // the local XBee sends this response on certain events, like association/dissociation
        
        /*if (msr.getStatus() == ASSOCIATED) {
          // yay this is great.  flash led
          flashLed(statusLed, 10, 10);
        } else if (msr.getStatus() == DISASSOCIATED) {
          // this is awful.. flash led to show our discontent
          flashLed(errorLed, 10, 10);
        } else {
          // another status
          flashLed(statusLed, 5, 10);
        }*/
      } else {
      	// not something we were expecting
        //flashLed(errorLed, 1, 25);    
      };
      for(int i=0;i<rx.getDataLength()/sizeof(float);i++){
        for(int j=0;j<sizeof(float);j++){
          temp[j]=rx.getData(i*sizeof(float)+j);
        }
        data[i]=*((float*)temp);
        Serial.print(data[i]);
        Serial.print(",");
      }
      Serial.println();
    } else if (xbee.getResponse().isError()) {
      Serial.print("Error reading packet.  Error code: ");  
      Serial.println(xbee.getResponse().getErrorCode());
    }


   //sd(data);
  // displaylcd(data);
  
 //Serial.println(data);
  }
